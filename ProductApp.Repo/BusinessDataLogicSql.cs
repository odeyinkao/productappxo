﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ProductApp.Data;
using ProductApp.Domain;
using System.Linq;

namespace ProductApp.Repo
{
    public class BusinessDataLogic
    {
        private ProductContext _context;

        public BusinessDataLogic(ProductContext context)
        {
            _context = context;
        }
        public BusinessDataLogic()
        {
            _context = new ProductContext();
        }


        public void InitializeDBWithSampleData()
        {
            var category1 = new Category { Name = "TV" };
            var category2 = new Category { Name = "Game" };
            _context.AddRange(category1, category2);
            _context.SaveChanges();

            var feature1 = new Feature { Name = "InternetConnection" };
            var feature2 = new Feature { Name = "Color" };
            var feature3 = new Feature { Name = "Refurbish" };
            _context.AddRange(feature1, feature2, feature3);
            _context.SaveChanges();


            var product1 = new Product { ProductName = "Sansumg", Category = category1, price = 999.99 };
            var product2 = new Product { ProductName = "LG", Category = category1, price = 899.99 };
            var product3 = new Product { ProductName = "PS4", Category = category2, price = 499.99 };
            var product4 = new Product { ProductName = "XBOX", Category = category2, price = 399.99 };
            _context.AddRange(product1, product2, product3, product4);
            _context.SaveChanges();

            var pf1 = new ProductFeature { Feature = feature1, Product = product1, Value = "Yes" };
            var pf2 = new ProductFeature { Feature = feature1, Product = product2, Value = "No" };
            var pf3 = new ProductFeature { Feature = feature1, Product = product3, Value = "Yes" };
            var pf4 = new ProductFeature { Feature = feature1, Product = product4, Value = "Yes" };
            var pf5 = new ProductFeature { Feature = feature2, Product = product1, Value = "Black" };
            var pf6 = new ProductFeature { Feature = feature2, Product = product2, Value = "Black" };
            var pf7 = new ProductFeature { Feature = feature2, Product = product3, Value = "Red" };
            var pf8 = new ProductFeature { Feature = feature2, Product = product4, Value = "Green" };
            var pf9 = new ProductFeature { Feature = feature3, Product = product1, Value = "Yes" };
            var pf10 = new ProductFeature { Feature = feature3, Product = product2, Value = "No" };
            var pf11 = new ProductFeature { Feature = feature3, Product = product3, Value = "No" };
            var pf12 = new ProductFeature { Feature = feature3, Product = product4, Value = "No" };
            _context.AddRange(pf1, pf2, pf3, pf4, pf5, pf6, pf7, pf8, pf9, pf10, pf11, pf12);
            _context.SaveChanges();
            // System.Console.Write(pf1);

        }

        // ⦁	To find product(s) which price is(are) over 500$.
        //⦁	To find product(s) which is(are) refurbished and color is not black.
        public List<Product> FilterProduct(ProductFilter filter)
        {
            var query = _context.Products.AsQueryable();

            if (filter != null)
            {
                query = FilterByName(filter, query);

                query = FilterProductsWithPrice(filter, query);

                query = FilterProductsWithFeatures(filter, query);

                query = FilterProductsWithCategory(filter, query);
            }

            return query.ToList();
        }

        //Todo convert to %like like syntax
        private static IQueryable<Product> FilterByName(ProductFilter filter, IQueryable<Product> query)
        {
            if (!String.IsNullOrEmpty(filter.ProductName))
            {
                query = query.Where(p => p.ProductName.ToLower().Equals(filter.ProductName.Trim().ToLower()));
            }

            return query;
        }

        private static IQueryable<Product> FilterProductsWithCategory(ProductFilter filter, IQueryable<Product> query)
        {
            if (filter.CategoryId != 0)
            {
                query = query.Where(p => p.CategoryId == filter.CategoryId);
            }
            else
            {
                if (!String.IsNullOrEmpty(filter.CategoryName))
                {
                    query = query.Where(p => p.Category.Name.ToLower().Equals(filter.CategoryName.Trim().ToLower()));
                }
            }

            return query;
        }

        private static IQueryable<Product> FilterProductsWithFeatures(ProductFilter filter, IQueryable<Product> query)
        {
            if (filter.FeatureCondition != null)
            {
                foreach (var filterCondition in filter.FeatureCondition)
                {
                    if (filterCondition.FeatureId != 0)
                    {
                        if (filterCondition.IsNot)
                        {
                            query = query.Where(s => s.ProductFeatures.Any(j => j.Feature.FeatureId == filterCondition.FeatureId && j.Value.ToLower() != filterCondition.Match.Trim().ToLower()));
                        }
                        else
                        {

                            query = query.Where(s => s.ProductFeatures.Any(j => j.Feature.FeatureId == filterCondition.FeatureId && j.Value.ToLower() == filterCondition.Match.Trim().ToLower()));
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(filterCondition.FeatureName))
                        {
                            if (filterCondition.IsNot)
                            {
                                query = query.Where(s => s.ProductFeatures.Any(j => j.Feature.Name.ToLower() == filterCondition.FeatureName.ToLower().Trim() && j.Value.ToLower() != filterCondition.Match.Trim().ToLower()));
                            }
                            else
                            {
                                query = query.Where(s => s.ProductFeatures.Any(j => j.Feature.Name.ToLower() == filterCondition.FeatureName.ToLower().Trim() && j.Value.ToLower() == filterCondition.Match.Trim().ToLower()));
                            }
                        }
                    }
                }
            }

            return query;
        }

        private static IQueryable<Product> FilterProductsWithPrice(ProductFilter filter, IQueryable<Product> query)
        {
            if (filter.PriceValue != 0)
            {
                switch (filter.PriceDirection.ToLower())
                {
                    case PriceDirection.Greater:
                        query = query.Where(p => p.price > filter.PriceValue);
                        break;
                    case PriceDirection.Lesser:
                        query = query.Where(p => p.price < filter.PriceValue);
                        break;
                    case PriceDirection.Equal:
                        query = query.Where(p => Math.Abs(p.price - filter.PriceValue) < 0.01);
                        break;
                    default:
                        query = query.Where(p => Math.Abs(p.price - filter.PriceValue) < 0.01);
                        break;
                }
            }

            return query;
        }
    }
}
