﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Domain
{
    public class Product
    {
        public Product()
        {
            ProductFeatures = new List<ProductFeature>();
        }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public int CategoryId { get; set; }
        public Category Category { get; set; }
        public double price { get; set; }
        public List<ProductFeature> ProductFeatures { get; set; }
    }
}
