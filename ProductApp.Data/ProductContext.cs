﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using ProductApp.Domain;
using Microsoft.EntityFrameworkCore;

namespace ProductApp.Data
{

    public class ProductContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductFeature> ProductFeatures { get; set; }

        public static readonly ILoggerFactory ConsoleLoggerFactory
         = LoggerFactory.Create(builder =>
         {
             builder
               .AddFilter((category, level) =>
                   category == DbLoggerCategory.Database.Command.Name
                   && level == LogLevel.Information)
               .AddConsole();
         });


        public ProductContext()
        { }

        public ProductContext(DbContextOptions options)
          : base(options)
        { }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                .UseLoggerFactory(ConsoleLoggerFactory)
                 .EnableSensitiveDataLogging(true)
                  .UseSqlServer("Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=ProductDB;Data Source=DESKTOP-2D995CA");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductFeature>().HasKey(pf => new { pf.ProductId, pf.FeatureId });
        }
    }
}
