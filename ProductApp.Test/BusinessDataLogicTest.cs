using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.EntityFrameworkCore;
using ProductApp.Data;
using ProductApp.Domain;
using ProductApp.Repo;
using System.Collections.Generic;
using System.Diagnostics;

namespace ProductApp.Test
{
    [TestClass]
    public class BusinessDataLogicTest
    {
        [TestMethod]
        public void QueryAllProducts()
        {
            var repo = new BusinessDataLogic();
            using (var context = new ProductContext())
            {
                // repo.InitializeDBWithSampleData();
                List<Product> products = new List<Product>();
                products = repo.FilterProduct(null);
                Assert.AreEqual(products.Count, 4);
            }
        }

        [TestMethod]
        public void Query_Product_With_Price_Greater_Than_500()
        {
            var repo = new BusinessDataLogic();
            var filter = new ProductFilter();
            filter.PriceValue = 500;
            filter.PriceDirection = "Greater";
            using (var context = new ProductContext())
            {
                List<Product> products = new List<Product>();
                products = repo.FilterProduct(filter);
                Assert.AreEqual(products.Count, 2);
            }
        }

        [TestMethod]
        public void Query_Product_With_Feature_Refurbished_And_Color_IsNot_Black()
        {
            var repo = new BusinessDataLogic();
            var filter = new ProductFilter();
            var refurbished = new FeatureCondition();
            refurbished.FeatureId = 3;
            refurbished.Match = "Yes";
            var colorIsNotBlack = new FeatureCondition();
            colorIsNotBlack.FeatureName = "Color";
            colorIsNotBlack.Match = "Black";
            colorIsNotBlack.IsNot = true;

            filter.FeatureCondition.Add(refurbished);
            filter.FeatureCondition.Add(colorIsNotBlack);
            using (var context = new ProductContext())
            {
                List<Product> products = new List<Product>();
                products = repo.FilterProduct(filter);
                Assert.AreEqual(products.Count, 0);
            }
        }
    }
}

