﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Domain
{
   public class ProductFeature
    {
        public int FeatureId { get; set; }
        public int ProductId { get; set; }
        public string Value { get; set; }
        public Feature Feature { get; set; }
        public Product Product { get; set; }
    }
}
