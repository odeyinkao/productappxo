﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Domain
{
    public class Feature
    {
        public Feature()
        {
            ProductFeatures = new List<ProductFeature>();
        }
        public int FeatureId {get; set;}
        public string Name { get; set; }
        public List<ProductFeature> ProductFeatures { get; set; }
    }
}
