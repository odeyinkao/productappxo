﻿using System;
using ProductApp.Data;
using ProductApp.Repo;

namespace ProductApp.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            var _logic = new BusinessDataLogic();
            _logic.InitializeDBWithSampleData();

        }
    }
}
