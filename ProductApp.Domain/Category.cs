﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Domain
{
    public class Category
    {
        public Category()
        {
            Products = new List<Product>();
        }
        public int CategoryId {get; set;}
        public string Name { get; set; }
        public List<Product> Products { get; set; }
    }
}
