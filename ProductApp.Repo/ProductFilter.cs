﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductApp.Repo
{
    public class ProductFilter
    {
        public ProductFilter()
        {
            FeatureCondition = new List<FeatureCondition>();
        }
        public string ProductName { get; set; }
        public int CategoryId { get; set; }
        public String CategoryName { get; set; }
        public double PriceValue { get; set; }
        public String PriceDirection { get; set; }
        public List<FeatureCondition> FeatureCondition { get; set; }
    }


    public class FeatureCondition
    {
        public int FeatureId { get; set; }
        public string FeatureName { get; set; }
        public string Match { get; set; }
        public bool IsNot { get; set; }
    }

    static class PriceDirection
    {
        public const string Greater = "greater";
        public const string Lesser = "lesser";
        public const string Equal = "equal";
    }
}
